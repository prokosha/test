class ConcatIMG {
    constructor(selector) {
        this.selector = selector + ".api_cnct-img";
        this.checkbox = document.querySelector(this.selector + ' .vertical_mode');
        this.imgInput1 = document.querySelector(this.selector + ' .imageLoader1');
        this.imgInput2 = document.querySelector(this.selector + ' .imageLoader2');
        this.btnDelete = document.querySelector(this.selector + ' .button_remove');
        this.cntCanvas = document.querySelector(this.selector + " .cnt_canvas");
        this.canvas = document.querySelector(this.selector + " .canvas");
        this.ctx = this.canvas.getContext('2d');
        this.btnDelete.addEventListener('click', this.deleteLastImg.bind(this));
        this.imgInput1.addEventListener('change', this.handleImage.bind(this));
        this.imgInput2.addEventListener('change', this.handleImage.bind(this));
        this.checkbox.addEventListener('change', this.switchModes.bind(this));
        this.checkbox.addEventListener('change', this.cleanObj.bind(this));
        this.cntCanvas.addEventListener('mousewheel', this.horizontalScroller.bind(this));
        this.cntCanvas.addEventListener("DOMMouseScroll", this.horizontalScroller.bind(this));
        this.width = {
            imgLoader1: [],
            imgLoader2: [],
            gallery: 0
        }
        this.height = {
            imgLoader1: [],
            imgLoader2: [],
            gallery: 0
        }
        this.stateCheckbox = false;
        const widthApi = document.querySelector(this.selector).offsetWidth;
        this.constParamOfSize = widthApi * (widthApi < 397 ? 0.8 : 0.5);
        this.switchModes();
    }
    setQualityImg(state) {
        this.ctx.mozImageSmoothingEnabled = state;
        this.ctx.webkitImageSmoothingEnabled = state;
        this.ctx.msImageSmoothingEnabled = state;
        this.ctx.imageSmoothingEnabled = state;
    }
    switchModes() {
        this.stateCheckbox = this.checkbox.checked;
        if (!this.stateCheckbox) {
            this.canvas.height = this.constParamOfSize;
            this.canvas.width = 30000;
        } else {
            this.canvas.width = this.constParamOfSize;
            this.canvas.height = 30000;
        }
        if (this.btnDelete.offsetWidth < 53) {
            document.querySelector(this.selector + " .button_remove p").textContent = "Del";
        }
    }
    cleanObj() {
        const typeObj = this.getTypeObjData(false);
        for (const key in this[typeObj]) {
            const valKey = this[typeObj][key];
            Array.isArray(valKey) ? valKey.length = 0 : this[typeObj][key] = 0;
        }
    }
    getTypeObjData(check) {
        if (check) {
            return this.stateCheckbox ? 'height' : 'width';
        } else {
            return this.stateCheckbox ? 'width' : 'height';
        }
    }
    insertBefore(a, b, paramCurrImg, shiftForInput2, scaleParam) {
        this.setQualityImg(false);
        const cutImg = this[scaleParam]['imgLoader' + a]
            .slice(this[scaleParam]['imgLoader' + b].length + shiftForInput2)
            .reduce((acc, el) => acc + el);
        const clipСoordinate = this[scaleParam].gallery - cutImg;
        if (scaleParam === 'height') {
            this.ctx.drawImage(this.canvas, 0, clipСoordinate, this.constParamOfSize, cutImg, 0, clipСoordinate + paramCurrImg, this.constParamOfSize, cutImg);
        } else {
            this.ctx.drawImage(this.canvas, clipСoordinate, 0, cutImg, this.constParamOfSize, clipСoordinate + paramCurrImg, 0, cutImg, this.constParamOfSize);
        }
        return clipСoordinate;
    }
    drawImage({ target }, typeInput) {
        if (target.files && target.files[0]) {
            const reader = new FileReader();
            reader.addEventListener("load", ({ target }) => {
                const img = new Image();
                img.addEventListener("load", () => {
                    const scaleWidth = img.width * this.constParamOfSize / img.height;
                    const scaleHeight = img.height * this.constParamOfSize / img.width;
                    const typeObj = this.getTypeObjData(true);
                    const typeScaleParam = typeObj === 'height' ? scaleHeight : scaleWidth;
                    const imgLoader1Lenght = this[typeObj].imgLoader1.length;
                    const imgLoader2Lenght = this[typeObj].imgLoader2.length;
                    const conditionСheckFirst = typeInput === 'imgLoader1' && imgLoader1Lenght < imgLoader2Lenght;
                    const conditionСheckSec = typeInput === 'imgLoader2' && imgLoader1Lenght - imgLoader2Lenght > 1;
                    const changeCoord = conditionСheckFirst ? this.insertBefore('2', '1', typeScaleParam, 0, typeObj) :
                        (conditionСheckSec ? this.insertBefore('1', '2', typeScaleParam, 1, typeObj) : this[typeObj].gallery);
                    this.setQualityImg(true);
                    this.stateCheckbox ? this.ctx.drawImage(img, 0, changeCoord, this.constParamOfSize, scaleHeight) :
                        this.ctx.drawImage(img, changeCoord, 0, scaleWidth, this.constParamOfSize);
                    this[typeObj].gallery += typeScaleParam;
                    this[typeObj][typeInput].push(typeScaleParam);
                });
                img.src = target.result;
            });
            reader.readAsDataURL(target.files[0]);
        }
    }
    deleteLastImg() {
        const typeRendering = this.getTypeObjData(true);
        const imgLoader1 = this[typeRendering].imgLoader1;
        const imgLoader2 = this[typeRendering].imgLoader2;
        const typeInput = imgLoader1.length > imgLoader2.length ? 'imgLoader1' : 'imgLoader2';
        const arrForUsing = this[typeRendering][typeInput];
        const paramLastImg = arrForUsing[arrForUsing.length - 1];
        this.stateCheckbox ? this.ctx.clearRect(0, this[typeRendering].gallery - paramLastImg, this.constParamOfSize, paramLastImg) :
            this.ctx.clearRect(this[typeRendering].gallery - paramLastImg, 0, paramLastImg, this.constParamOfSize);
        arrForUsing.pop();
        this[typeRendering].gallery -= paramLastImg || 0;
    }
    handleImage(event) {
        this.drawImage(event, event.target === this.imgInput1 ? 'imgLoader1' : 'imgLoader2');
        event.target.value = "";
    }
    horizontalScroller(event) {
        if (this.stateCheckbox) {
            this.cntCanvas.scrollLeft = 0;
        } else {
            const delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));
            this.cntCanvas.scrollLeft -= (delta * 40);
            event.preventDefault();
        }
    }
}
new ConcatIMG('.example');